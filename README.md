# [azion-developer-challenge](https://bitbucket.org/RicardoGosch/azion-developer-challenge)

This project is a challenge developed for the company Azion from Porto Alegre. It consists of creating a simple auto-complete solution in a series and movie search.

The use of Bootstrap is required, and the use of JQuery is optional. Both were used.

## Setup

1. Install [`Node.js`](https://nodejs.org/) and [`npm`](http://blog.npmjs.org/post/85484771375/how-to-install-npm).
1. Run `npm install gulp -g`.
2. Run `npm install`.

## Starting

1. Run `gulp`
2. Open on browser [`http://localhost:3000`](http://localhost:3000)