$(function() {
	/*
		Type:
		  0 - Movie
		  1 - Serie
	*/
	var typeElement  = $('select#type');
	var valueElement = $('input#name');
	var list         = $('ul.list-group#auto-complete');


	valueElement.keyup(function(event) {
		switch(event.keyCode){
			case 40:
			case 38:
			case 27:
			case 13:
				event.preventDefault();
				break;
			default:
				run();
		}
	});

	typeElement.change(function(event) {
		run();
	});

	function run(){
		var type = typeElement.val();

		if(valueElement.val() == ""){
			list.html('');
			return;
		}

		writelist(getcontains(valueElement.val(), type));

		$(document).trigger('autoComplete');
	}

	typeElement.change(function(event) {
		
	});

	function writelist(data){
		list.html('');
		for(var i = 0; i < Object.keys(data).length ; i++){
			list.append('<li class="list-group-item">'+ data[i].name +'</li>');
		}
		if(!Object.keys(data).length){
			list.html('');
		}
	}	

	// Return the data with contain
	function getcontains(value, type) {
		var data = getdata();
		var turn = {};
		var count = 0;

		for(var i = 0; i < Object.keys(data).length ; i++){
			var object = data[i];
			if(object.type == type && search(object, value) >= 0) {
				turn[count] = object;
				count++;
			}
		}
		return turn;
	}

	function search(element, value){
		var name = element.name.toLowerCase();

		return name.indexOf(value.toLowerCase());
	}

	// Return data of movies and series (replaces the request with an API)
	function getdata(){
		return [
			{
				name: "Napoleon Dynamite",
				type: 0
			},
			{
				name: "Across the Universe",
				type: 0
			},
			{
				name: "The Walking Dead",
				type: 1
			},
			{
				name: "Game of Thrones",
				type: 1
			}
		]
	}
	
	$('form').submit(function(event) {
		event.preventDefault();
	});
});