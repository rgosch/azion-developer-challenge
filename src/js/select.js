$(function() {

	var valueElement = $('input#name');
	var list         = $('ul.list-group#auto-complete');
	var selected     = "selected";

	$(document).on('autoComplete', function(event) {
		selectFirst();
	});

	list.mouseover(function(event) {
		var target = event.target;
		unselectAll();
		select($(target));
	});

	list.click(function(event) {
		fill();
	});

	valueElement.focusin(function(event) {
		list.css({
			opacity: 1,
			pointerEvents: 'auto'
		});
	});

	valueElement.focusout(function(event) {
		// document.setTi
		setTimeout(function(){
			list.css({
				opacity: 0,
				pointerEvents: 'none'
			});
		}, 300);
	});

	valueElement.keyup(function(event) {
		switch(event.keyCode){
			case 40:
				// Key down
				if(lastSelected()) return;
				if(!getselected().length){
					selectFirst();
				} else {
					getselected().removeClass(selected).next().addClass(selected);
				}
				break;
			case 38:
				// Key up
				if(firstSelected()) return;
				getselected().removeClass(selected).prev().addClass(selected);
				break;
			case 27:
				// Key esc
				unselectAll();
				break;
			case 13:
				// Key fill
				fill();
				break;
		}
	});

	$('ul li.list-group-item').mouseenter(function(event) {
		alert('aaa');
	});


	function firstSelected(){
		var first = list.find('li:first-child');

		return first.hasClass(selected);
	}

	function lastSelected(){
		var last = list.find('li:last-child');

		return last.hasClass(selected);
	}

	function selectFirst(){
		var first = list.find('li:first-child');

		first.addClass(selected);
	}

	function unselectAll(){
		var all = list.find('li');

		all.removeClass(selected);
	}

	function getselected(){
		return list.find('li.'+selected);
	}

	function fill(){
		var select = getselected();

		valueElement.val(select.text());
	}

	function select(item){
		item.addClass(selected);
	}

	function unselect(item){
		item.removeClass(selected);
	}



});